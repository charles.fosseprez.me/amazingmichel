

var DEBUG=false;

print("***");

regen_dir = true;
print("Image format is set to: String");
var im_format = "tiff";
print("Image second format?? is set to: String");
var im_format = "tiff";
print("Image type is set to: String");
var im_type = "16-bit";
print("Image clean set to: Bool");
var clean=false;
print("Image Dirty side set to: String");
var dirty_side="Right";
print("Image dirty border set to: Bool");
var dirty_border=true;
print("Border dirtyness set to: Number");
var border_dirtyness=50;
print("Tubules Width set to: Number");
var tubulewidth = 6;
print("Classif set to: Bool");
var doclassif = true;
print("Weka version set to: String");
var weka_version = 3233;
print("Model name set to: String");
var model_name = "classifier2.model";

print("Exclude small set to: Bool");

exclude_smallstuff = true;

print("Small tresh set to: Number");
small_tresh = 35;

print("Target analysis set to: String");
target_analysis = "result4"

print("Do final plot set to: String");
var do_finalplot = true

print("Samples list set to: String");
var samples_list = "s1@s9"



macro "macromichel Action Tool  - C97fD06D14D24D25D26D28D34D41D43D4eD52D55D65D71DebDf7C43fD19D1aD2aD2cD4dD85D93D94DbdDc3Dd4DdbDe6De7De8De9CddfD37D3eD59D77D9fDddDe3Df5DfaC22fD2bD3aD4bD8cD95Da4Da8DadDb4Db8DbcDc4DccDd6Dd7Dd8Dd9DdaCaafD0aD36D45D46D47D49D58D5aD63D67D73D75D87D88Df6Df9C66fD4aD5bD66D69D76D82D86D89D91D92D96D97Da1Dc2Dd3DdcCfffD00D01D02D03D0cD0dD0eD0fD10D11D12D1dD1eD1fD20D21D2eD2fD30D3fD4fDb0DbfDc0DcfDd0Dd1DdeDdfDe0De1De2DedDeeDefDf0Df1Df2Df3Df4DfbDfcDfdDfeDffC21fD3bD3cD4cD5dD6aD6dD7aD7dD8dD9dDa5Db5Dc5Dc6Dc7DcbCa9fD27D29D35D38D53D54D57D62D64D72D74D78Db1DbeDf8C53fD16D1bD3dD5eD68D6eD7eD83D8eD9eDa2DaeDb2DcdDe5DeaCeefD04D0bD40D50D5fDa0DafDc1DceDecC24fD5cD6bD6cD79D7bD7cD8bD98D99Da3Da7Da9Db3Dd5CcbfD05D13D1cD22D2dD31D39D48D60D6fD70D7fD80D8fD90Dd2C86fD07D08D09D15D17D18D23D32D33D42D44D51D56D61D81D84De4C01fDa6DaaDb6Db7Db9DbaDc8" {





Dialog.create("Image param");
Dialog.addCheckbox("Regenerate the directories:   ", true);
Dialog.addChoice("Format (Sometimes, Majuscule sometimes not!):   ", newArray("tiff", "tif", "png", "TIF"), "TIF");
Dialog.addChoice("Second Format (Because.. Needed after classif, maybe cause of LUT?):   ", newArray("tiff", "tif", "png", "TIF"), "tif");
Dialog.addChoice("Type:   ", newArray("8-bit", "16-bit", "32-bit", "RGB"), "16-bit");
Dialog.addCheckbox("Clean:   ", true);
Dialog.addChoice("Dirty part:   ", newArray("None", "Left", "Right"), "Right");
Dialog.addCheckbox("Dirty Border:   ", true);
Dialog.addNumber("Border dirtyness (pixels):  ", 50);
Dialog.addNumber("Tubule Width:  ", 6);
Dialog.addCheckbox("Use classification:   ", true);
Dialog.addString("Weka version (visible from weka window in ImageJ):   ", "3.2.33");
Dialog.addChoice("Model name:   ", newArray("classifier0.model", "classifier1.model", "classifier2.model"), "classifier2.model");
Dialog.addCheckbox("Exclude small from classification:   ", true);
Dialog.addNumber("Treshold exclusion:  ", 75);
Dialog.addChoice("Analysis target folder:   ", newArray("resultclassif", "result3", "result4"), "resultclassif");
Dialog.addCheckbox("Do final plotting:   ", true);
Dialog.addString("Number samples (Delimited by @. Max 9. For example s1@s9):   ", "s1@s3");
Dialog.show();
regen_dir = Dialog.getCheckbox();
im_format = Dialog.getChoice();
sec_format = Dialog.getChoice();
im_type = Dialog.getChoice();
clean = Dialog.getCheckbox();
dirty_side = Dialog.getChoice();
dirty_border = Dialog.getCheckbox();
border_dirtyness = Dialog.getNumber();
tubulewidth = Dialog.getNumber();
doclassif = Dialog.getCheckbox();
weka_version = Dialog.getString();
model_name = Dialog.getChoice();
exclude_smallstuff = Dialog.getCheckbox();
small_tresh = Dialog.getNumber();
target_analysis = Dialog.getChoice();
do_finalplot = Dialog.getCheckbox();
samples_list = Dialog.getString();




////////////////////////////////////////////////////////////////////////////////
// Data System
////////////////////////////////////////////////////////////////////////////////
print("***");
print("ImageJ version:", call("ij.IJ.getVersion")); // same as getVersion()
print("java.version:", call("java.lang.System.getProperty", "java.version"));
print("user.home:", call("java.lang.System.getProperty", "user.home"));
print("----------------------------------------------------------------------");
print("Time:", call("java.lang.System.currentTimeMillis")); // returns a string
print("Free memory:", call("ij.IJ.freeMemory")); // returns a string
print("Armement des tobogans: ON"); // returns bullshit string  
print("Max memory:", parseInt(call("ij.IJ.maxMemory"))/(1024*1024)+"MB");
print("***");
////////////////////////////////////////////////////////////////////////////////




print("!!!!!");
print("to change environment, turn DEBUG (line 15) to false/true");
print("path for DEBUG can be modifyed line 29?");
print("!!!!!");

if(DEBUG==true){
print("***");
print("Environement is: DEBUG");
print("***");
batch_opt=false;
//Path separator should be /
input = "C:/Users/charli/Desktop/michel_counter/sample";
print("Path set to DEBUG: ",input);

} else {
print("***");
print("Environement is: GIVEME");
print("***");
batch_opt=true;
print("Please select the repository folder");
repo = getDirectory("Input directory (Repository location [Main folder downloaded])");
input = repo + "/" + "sample/";
print("Path selected: ",input);
}


//Batch mode supress vision of process, win time X10
setBatchMode(batch_opt);
print("---");
print("setBatchMode is:",batch_opt);
print("---");




//Macro template to process multiple images in a folder



mother = File.getParent(input);

function regenerate_directory(myDir){
  ok = File.delete(myDir);
  if (File.exists(myDir))
      exit("Unable to delete directory");
  else
      print("Directory and files successfully deleted");


  File.makeDirectory(myDir)

  }


print("Pre Processed Images will be located in clean folder at the same level than your sample folder");
clean_dir=mother+"/clean";

print("Model path is parent of sample, /model");
model_dir=mother+"/model";
model = model_dir+"/"+model_name;




print("Output will be located in result1 folder at the same level than your sample folder");
result1=mother+"/result1/";

print("Output will be located in result2 folder at the same level than your sample folder");
result2=mother+"/result2/";

print("Output will be located in result3 folder at the same level than your sample folder");
result3=mother+"/result3/";

print("Output will be located in result4 folder at the same level than your sample folder");
result4=mother+"/result4/";

print("Output will be located in result folder at the same level than your sample folder");
resultclassif=mother+"/resultclassif/";

print("Output will be located in ready folder at the same level than your sample folder");
ready1=mother+"/ready1/";

print("Output will be located in analyzed folder at the same level than your sample folder");
analyzed1=mother+"/analyzed1/";

if(regen_dir==true){
    print("> regen the dirs");

    regenerate_directory(clean_dir);

    regenerate_directory(result1);

    regenerate_directory(result2);

    regenerate_directory(result3);

    regenerate_directory(result4);

    regenerate_directory(resultclassif);

    regenerate_directory(ready1);

    regenerate_directory(analyzed1);

    }

if(clean==true){
    print("================================");
    print("Dirty time");
    // Preprocess Images
    preprocessFolder(input,clean_dir,DEBUG);
    // Shift input dir from sample to clean
    input = clean_dir;
    print("Victory over the pre processing!");
    print("================================");
}


function preprocessFolder(input,clean_dir,DEBUG) {


    // Get list of files in folder
    list0 = getFileList(input);
    // Prepare Dir
    input1=input+"/";
    clean1=clean_dir+"/";



    for (i = 0; i < list0.length; i++) {

        name_im = list0[i];
        print("File name:" + name_im);


        // Check that file is specified format
        if (endsWith(name_im, "."+im_format)) {
            print("> Preprocess:" + name_im);
            // Open image
            open(input1 + name_im);
            rename("ori");
            // Get size
            width = getWidth;
            height = getHeight;
            // duplicate left or  right

            if (dirty_side == "Right"){
                makeRectangle(0, 0, height, height);
            }
            if (dirty_side == "Left"){
                makeRectangle(height, 0, height, height);
            }
            run("Duplicate...", "title="+name_im);
            selectWindow("ori");
            close();
            selectWindow(name_im);
            if (dirty_border == true){
                rename("demi_cleaned");
                width = getWidth;
                height = getHeight;

                makeRectangle(border_dirtyness, border_dirtyness, height-(2*border_dirtyness), height-(2*border_dirtyness));

                run("Duplicate...", "title="+name_im);
                selectWindow("demi_cleaned");
                close();
            }
            save(clean_dir + "/" + name_im);
            close();

        }
    }
}




//Process all images in input folder
print("================================");
print("Processing time");

processFolder();

print("Victory over the processing");
print("================================");




function processFolder() {
    list0 = getFileList(input);
    input1=input+"/";

    result_dir1=result1;

    result_dir2=result2+"/";

    result_dir3=result3+"/";

    for (i = 0; i < list0.length; i++) {
        name_im=list0[i];

        print("> Process:" + name_im);
        print(name_im);

        open(input1 + name_im);
        //print("convert image to 8-bit");
        //run("8-bit");
        print("> processing image (method 1)");
        processIMAGE1();
        print("save output image");
        save(result_dir1 + name_im);
        print("close image");
        if(DEBUG==false){
            close();
        }

        open(input1 + name_im);
        print("> processing image (method 2)");
        processIMAGE2();
        print("save output image");
        save(result_dir2 + name_im);
        print("close image");
        if(DEBUG==false){
            close();
        }

        open(input1 + name_im);
        print("> processing image (method 2+)(ready for classification");
        processIMAGE2();
        rename("method2");
        stacking();
        new_name = name_im;
        rename(new_name);
        print("save output image");
        save(result_dir3 + new_name);
        selectWindow(new_name);
        close();


        open(input1 + name_im);
        print("> processing image (method 3)");
        processIMAGE3();
        print("save output image");
        save(result4 + name_im);
        print("close image");
        if(DEBUG==false){
            close();
        }



    }
}






function processIMAGE0() {
    width = getWidth;
    height = getHeight;

    run("Subtract Background...", "rolling=100");
    run("Kuwahara Filter", "sampling=5");

}


function processIMAGE1() {
    width = getWidth;
    height = getHeight;

    run("Normalize Local Contrast", "block_radius_x=20 block_radius_y=20 standard_deviations=20 center stretch");
    //run("8-bit");
    //run("Bilateral Filter", "spatial=3 range=50");
    //run("Median...", "radius=5");
    run("Kuwahara Filter", "sampling=5");
    run("Subtract Background...", "rolling=100");
}


function processIMAGE2() {
    width = getWidth;
    height = getHeight;

    run("Normalize Local Contrast", "block_radius_x=20 block_radius_y=20 standard_deviations=20 center stretch");
    //run("8-bit");
    //run("Bilateral Filter", "spatial=3 range=50");
    //run("Median...", "radius=5");
    run("Kuwahara Filter", "sampling=5");
    run("Subtract Background...", "rolling=100");
}



function processIMAGE3() {
    width = getWidth;
    height = getHeight;

    run("Subtract Background...", "rolling=10");
    run("Bandpass Filter...", "filter_large=50 filter_small=3 suppress=None tolerance=5 autoscale saturate");
    run("Make Binary");
}







function stacking() {
    width = getWidth;
    height = getHeight;

    open(input1 + name_im);
    rename("original");
    run("Subtract Background...", "rolling=100");

    run("Concatenate...", "open image1=original image2=method2 image3=[-- None --]");
    rename("concat");
    run("Z Project...", "projection=Median");
    selectWindow("concat");
    close();
    selectWindow("MED_concat");
}


if(doclassif==true){
    setBatchMode(false);
    print("================================");
    print("> Activate Weka");

    // Open sample
    // Get list of files in folder
    list = getFileList(input);
    open(input + "/" + list[0]);
    /////////////////////////////////////////
    //Weka activation
    /////////////////////////////////////////
    run("Trainable Weka Segmentation"); // start the plugin
    print("Wait a long time");
    wait (3000);

    selectWindow("Trainable Weka Segmentation v"+weka_version); // select the window
    print(">>> Start Classification");
    call("trainableSegmentation.Weka_Segmentation.loadClassifier", model);

    /////////////////////////////////////////
    //First image closing + weka selection
    /////////////////////////////////////////
    setBatchMode(true);
    print("Weka activated");
    print("================================");


    //Classify all images in result3 folder
    print("================================");
    print("Classification time");

    classifyFolder();

    print("Victory over the Classification");
    print("================================");

}


function classifyFolder() {

    result_dir3=result3+"/";

    list0 = getFileList(result_dir3);



    for (i = 0; i < list0.length; i++) {
        start = getTime();
        name_im=list0[i];

        print("> Classif:" + name_im);

        weka_classif(result_dir3, name_im);

        /////////////////////////////////////////
        //Binarisation (2 classes)
        /////////////////////////////////////////
        selectWindow("Classification result");
        run("8-bit");
        setOption("BlackBackground", false);
        run("Make Binary", "method=Default background=Default calculate");
        run("Invert");
        print(">>>Binarisation DONE");
        save(resultclassif+name_im);
        print(">>>Saving DONE");
        close();
        print("Classif image took: ");
        print((getTime()-start)/1000);


    }
    close_weka();
}





function weka_classif(input_folder, filename) {
    selectWindow("Trainable Weka Segmentation v"+weka_version); // select the window
    print(">>> Start Classification");
    call("trainableSegmentation.Weka_Segmentation.applyClassifier",input_folder ,filename, "showResults=true", "storeResults=false", "probabilityMaps=false","");
    print(">>>Classification DONE");

}

function close_weka() {

    selectWindow("Trainable Weka Segmentation v"+weka_version);
    close();

}




//Analyze all images in result_classif folder
print("================================");
print("Analysis time");

if (exclude_smallstuff == true){
    exclude_small();
    }
analyzeFolder();
analysisFinal();

print("Victory over the Analysis");
print("================================");




function exclude_small() {
    //setBatchMode(false);
    print("> Start cleaning small elements");
    to_analyze_folder=resultclassif;
    list0 = getFileList(to_analyze_folder);
    for (i = 0; i < list0.length; i++) {
        name_im=list0[i];
        abs_ori=to_analyze_folder+name_im;
        open(abs_ori);
        run("Invert LUT");
        run("Analyze Particles...", "size=0-"+small_tresh+" add");
        setBackgroundColor(255, 255, 255);
        setForegroundColor(255, 255, 255);
        roiManager("List");
        print(name_im + " did found this many small elements: ");
        print(Table.size());
        roiManager("Fill");

        roiManager("Deselect");
        roiManager("reset");
        //selectWindow("ROI Manager");
        //run("Select All");
        //roiManager("Deselect");
        //roiManager("Delete");
        //run("Clear Results");
        //run("Close");
        run("Invert LUT");
        save(abs_ori);
        //close();
        selectWindow("Overlay Elements");
        close();
}


function analyzeFolder() {
    if (target_analysis == "resultclassif"){
        to_analyze_folder=resultclassif;
        list0 = getFileList(to_analyze_folder);
        }
    if (target_analysis == "result4"){
        to_analyze_folder=result4;
        list0 = getFileList(to_analyze_folder);
        }
    if (target_analysis == "result3"){
        to_analyze_folder=result3;
        list0 = getFileList(to_analyze_folder);
        }


    for (i = 0; i < list0.length; i++) {
        // To see detected files
        name_im=list0[i];
        print("Detected:" + name_im);
        }
    for (i = 0; i < list0.length; i++) {

        name_im=list0[i];
        print("File name:" + name_im);
        if (endsWith(name_im, "."+sec_format)) {
            print("> Analyze:" + name_im);
            abs_ori=to_analyze_folder+name_im;
            abs_dest=ready1+name_im;
            abs_dest2=analyzed1+name_im;
            open(abs_ori);
            run("Invert LUT");
            run("Skeletonize");
            save(abs_dest);
            run("Invert");
            run("Analyze Skeleton (2D/3D)", "prune=none display");
            //saveAs("Results", abs_dest2+"_DetailledInfo_Branch information12.csv");
            selectWindow("Results");
            saveAs("Results", abs_dest2+"_Results_Branch information12.csv");

            // Make plot endvsjunc
            plot = Plot.create("Plot of Results", "# End-point voxels", "# Junctions");
            Plot.add("Circle", Table.getColumn("# End-point voxels", "Results"), Table.getColumn("# Junctions", "Results"));
            Plot.setStyle(0, "black,magenta,1.0,Circle");
            Plot.show();
            Plot.makeHighResolution("Plot of Results_",4.0);
            saveAs("PNG", abs_dest2+"_Plot_endvsjunc");
            close();
            selectWindow("Plot of Results");
            close();


            selectWindow("Tagged skeleton");
            save(abs_dest2);


            // Make results
            selectWindow("Results");
            tot_end=0;
            tot_junc=0;
            tot_branch=0;
            len=0;
            for (j=0; j < nResults; j++) {
                    tot_end = tot_end + getResult("# End-point voxels", j);
                    tot_junc= tot_junc + getResult("# Junctions", j);
                    tot_branch = tot_branch + getResult("# Branches", j);
                    len = len + getResult("Average Branch Length", j);
                    }
            av_len = len / nResults;


            name_table="Results_"+name_im;

            Table.create(name_table);
            selectWindow(name_table);
            //Table.size();
            Table.title(name_table);
            //Table.headings("# of Branches	# of Ends	# of Junctions   Average Length");
            Table.set("# of Branches", 0, tot_branch);
            Table.set("# of Ends", 0, tot_end);
            Table.set("# of Junctions", 0, tot_junc);
            Table.set("Average Length", 0, av_len);
            Table.update();

            Table.save(analyzed1+name_table+"@.csv");
            selectWindow(name_table);
            run("Close");
            run("Close All");


            }
        }
    }


function analysisFinal() {
    list0 = getFileList(analyzed1);

    samp = split(samples_list, "@");

    for (z = 0; z < samp.length; z++) {
        sam = samp[z];
        Table.create(sam);
        counter=0;
        print(">working on s:"+sam);
        for (i = 0; i < list0.length; i++) {
            // To see detected files
            name_file=list0[i];
            abs_name = analyzed1+name_file;

            if (endsWith(name_file, "@.csv")) {
                nome= split(name_file, "s");
                num = "s"+substring(nome[4], 0, 1);
                print("found:"+name_file);
                //print(nome[4]);

                //print(num);
                if (num == sam) {
                    print("is sam");
                    print(name_file);
                    tome= split(name_file, "t");
                    n="";
                    for (t=0; t<lengthOf(tome[2]); t++) {
                        c = substring(tome[2], t, t+1);
                        if (c>="0" && c<="9")
                            n = n + c;
                        }
                    time = parseInt(n);
                    Table.set("time", Table.size(), time);
                    Table.open(abs_name);
                    ends = Table.get("# of Ends", 0);
                    bran = Table.get("# of Branches", 0);
                    node = Table.get("# of Junctions", 0);
                    Table.rename("kiki");
                    selectWindow("kiki");
                    run("Close");
                    selectWindow(sam);
                    Table.set("# of Ends", counter, ends);
                    Table.set("# of Branches", counter, bran);
                    Table.set("# of Junctions", counter, node);
                    counter=counter+1;
                    Table.update();
                    }
                }
            }
        }

        print("Saving the result table");
        for (z = 0; z < samp.length; z++) {
            sam = samp[z];
            selectWindow(sam);
            Table.sort("time");
            Table.save(analyzed1+sam+".csv");
            }


        print("creating initial plot");
        Table.rename(samp[0], "Results");
        Plot.create("Plot of Results", "Time", "# End-point voxels");
        Plot.add("Circle", Table.getColumn("time", "Results"), Table.getColumn("# of Ends", "Results"));
        Plot.setStyle(0, "black,blue,1.0,Circle");


        color_list = newArray("magenta","green","red","orange","black","light-blue","light-green","pink");
        legend_string = samp[0] + "\n";

        print("adding all the sample");
        for (z = 1; z < samp.length; z++) {
            sam = samp[z];
            selectWindow(sam);
            Table.save(analyzed1+sam+".csv");
            Plot.add("Circle", Table.getColumn("time", sam), Table.getColumn("# of Ends", sam));
            Plot.setStyle(z, "black,"+color_list[z-1]+",1.0,Circle");
            legend_string = legend_string + samp[z] + "\n";

            }
        Plot.addLegend(legend_string);
        Plot.show();

        Plot.makeHighResolution("Plot of Ends number",4.0);
        saveAs("PNG", analyzed1+"_Totalplot_Ends");



        Plot.create("Plot of Results", "Time", "# Branch");

        Plot.add("Circle", Table.getColumn("time", "Results"), Table.getColumn("# of Branches", "Results"));
        Plot.setStyle(0, "black,blue,1.0,Circle");


        color_list = newArray("magenta","green","red","orange","black","light-blue","light-green","pink");
        legend_string = samp[0] + "\n";

        print("adding all the sample");
        for (z = 1; z < samp.length; z++) {
            sam = samp[z];
            selectWindow(sam);
            Table.save(analyzed1+sam+".csv");
            Plot.add("Circle", Table.getColumn("time", sam), Table.getColumn("# of Branches", sam));
            Plot.setStyle(z, "black,"+color_list[z-1]+",1.0,Circle");
            legend_string = legend_string + samp[z] + "\n";

            }

        Plot.addLegend(legend_string);
        Plot.show();

        Plot.makeHighResolution("Plot of Ends number",4.0);
        saveAs("PNG", analyzed1+"_Totalplot_Branchs");

    }
}