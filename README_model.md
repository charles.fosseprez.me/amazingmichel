# Info

Here are the models.
They were trained on Fiji embeded plugin *trainable Weka segmentation v3.2.33*
  
## Training data

Data used for training is there as *data.arff*.
Images where processed untill *reasy_for_classif* before classif.
Yo can import it in *Weka trainable segmentation* if you want to train your own model.  

class 1 is the microtubules.
class 2 is the background.



## About the models

- *classifier2.model* work good. It is a RandomForest. Slow but here no need for speed.

- The *classifier0.model* and *classifier1.model* are not very stable. They are Fast random forest, trained on small data.

:cactus: Retro compatibility between Weka versions is problematic. 
You might need to train your model again from the data.arff if you have a different version of Weka.

For *classifier2.model* I used following model

![Alt text](./images_README/model_type.png?raw=true "find version")
  
And following features

![Alt text](./images_README/features.png?raw=true "find version")
  
I did balance the classes?
    

## Train your own models

You can try to train your own model. Just replace the one existing in the *model* folder.
(give name classifier0.model to your model for example) and use it during the macro.

