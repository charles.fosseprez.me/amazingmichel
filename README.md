***macromichel v0.1***  
*"I have a different constitution, I have a different brain, I have a different heart. I got tiger blood, man. Dying's for fools, dying's for amateurs."*  
--Charlie Sheen, 1 March  2011"  
    
**Author:** Charles Fosseprez  

**Contact:** Charles.fosseprez.me@gmail.com  

# Info

Here is some trial to automatically analyse microtubules from microscope observations with Fiji (ImageJ).
  
:cactus: This repository is a first draft.
  
:cactus: :cactus: The macro is folder dependant. Meaning that you will 
need to copy your data in the */sample* folder of the repository. The witchcraft will operate from there.

The macro have several update possibility quite easily. Lets see.
These upgrade include:
- adding result method in separate macro, so easy to modify
- proper separation of classification from processing, so can be skipped
- Supress the need of given the path repo at each run

:thumbsup: Please if you use it in your research cite the repository or the author.
  
## Basics
  

  
A Fiji macro was written to do the image processing. First get Fiji.
[Download Fiji](https://imagej.net/Fiji/Downloads)  
  
Fiji is a version of ImageJ including a lot of plugins.
  
  
To execute this macro, various way are possible. 
More of that later.    
[Intro to macro programming](https://imagej.net/Introduction_into_Macro_Programming)  
[About shortcuts](https://imagejdocu.tudor.lu/keyboard/start)  

## Pictures
  
The pictures of microtubules were aquired as Christel knows... [add details plz]
  


:bomb: Image I have as sample have a 2 images combined together, left observation, right black. I split them.  
This is the first part of the macro. Called preprocessing.  
  
![Alt text](./images_README/sample_0.png?raw=true "not good images combined")

:bomb: The image to be processed should only contain the image (no black background out of the image)
This can cause issue with downstream processes.  

:thumbsup: Here is an example of proper image to be processed:  

![Alt text](./images_README/sample_ready.png?raw=true "good image")
  

  

  

# Utilization

Various steps are required in order to use this macro.    

## Get the folder
The macro is folder dependant. 
It mean that it will only work from the folder obtain on the repository.
You can download the repository at:  
https://gitlab.com/charles.fosseprez.me/michel_counter  

### With git  
To get the repository I recommend to use *Git*.
With it you can directly clone the repository on your local machine.
Once you have git you just need to go to the place where you want the folder
to be located and enter in your console:  
```git
git clone https://gitlab.com/charles.fosseprez.me/michel_counter.git
```
### Without git
Else you can just download it with the download button present on the repository page.
  
![Alt text](./images_README/download_repo.png?raw=true "good image")


This will download the repository.
But then not sure how you can push back if you do upgrades.

### (git push) Issue with model files
*.model* and *.rar* arr big, might cause issue while pushing.
If you get an error:
```bash
error: RPC failed; curl 92 HTTP/2 stream 0 was not closed cleanly: PROTOCOL_ERROR (err 1)
```
You can also try to use the following command,
 replacing #BYTES_SIZE with the maximum file size for example 450000000.
```bash
git config --global http.postBuffer #BYTES_SIZE
```
This will increase your buffer size.

If this is not working. One can split the model folder into various .rar
arcihves. Then you will need to unzip them before being able to run the macro.
## Install the Macro for once
Once you have the folder, you can install the macro into fiji.
To do so open *Fiji* and go to the submenu *Plugins*, then *Macros*
as shown in the next image.
![Alt text](./images_README/install_macro.png?raw=true "how to install a macro")
  
From there select the file *macromichel.ijm*
  
Once the macro is installed it will be accessible from Fiji.
As the macro was designed as a tool, it will appear in the Fiji
main toolbar. 
![Alt text](./images_README/installed_macro.png?raw=true "macro installed")

You just need to click on the green logo to start the macro.
  
But at each start you will need to reinstall the macro.. Tired of that?
See the next chapter.

## Install Macro forever (recommended :thumbsup: )

### Put your macro in ImageJ AutoRun folder 
You need to modify and copy the *AutoInstall.ijm* file present in the repository.
 
First modify *AutoInstall.ijm* with your path by replacing the *path* in the following example
to the path to your repo (:cactus: BE CAREFUL about the \\\\ and not \\, also add the " at begining and start of the path):
```java
macro "AutoRun" {
        path="C:\\Users\\charli\\Desktop\\michel_counter\\macromichel.ijm";
        path_gut=replace(path, "\\","/");
        path_sergut="["+path_gut+"]";"
        run("Install...", "install="+path_sergut);
}
```
Then copy *AutoInstall.ijm* to you local *Fiji* folder at:  
**\Fiji.app\macros\AutoRun**  
Once this done, the Macro will be installed at start, and so the green cross will be there.
  
## If one intend to tinker
*Not really implemented in this macro*
If one intend to tinker the macro, the change of the value  
```java
DEBUG=false; 
```
to  
```java
DEBUG=true;
```
allow to trigger the DEBUG mode that will display all the crap you need!  


## Run Macro
  
Your data need to be put in sample. The results will appear in: 
- result1: Data (cleaned) and background substracted
- result2: Data for counting
- result3: Data for segmentation
- resultclassif: Result segmentation
- ready1: Data ready for analysis

Once run the macro will ask you different option, they speak for themself.


![Alt text](./images_README/options.png?raw=true "parameters macro")

- Format: is the format of the input images
- Second Format: Because some weird shit in Fiji. It change *.TIF* to *.tif* and stuff like that
- Clean: if there is a cleaning step. Set dirty part to none if no need to supress one half of the images.
- Border dirtyness: will supress the border of the images
- Tubules width: do nothing yet
- Use classification: if you want to run the classifier or not
- Weka version: can be found after launching the *Weka trainable segmentation* plugin, see following image.

![Alt text](./images_README/version.png?raw=true "find version")

  
After this step it will ask you the location of the repository.
Just point to thhe repository and the magic will operate then.

# Algorithmic

## Basic steps  
```mermaid
graph TD;
  A[Prepare image to standard]-->B[Put images in folder];
  B-->C[Launch the macro via Fiji];
```

  
Here is a little description of the algorythmic path that the macro will follow
  
```mermaid
graph TD
  subgraph "Main Processing"
  A[Open image]-->B
  B[Clear Background]-->C
  C[Extract Microtubules]-->D
  D[Count]-->E 
  E[Save result] 
end
```

In fact it dont follow this path for various reasons
  
The followed algorithmic can be easely described (we can discuss of that) for a publication.
The use of classifier might not be well perceived without a validation
of it via a test set (we can discuss of that).
But the use of the classifier is a little bit tan overkill here, 
if the goal is just an identification of the microtubule quantity.


# Classifier

To see more info on classifier see */README_model.md* file.
They are slow, but here speed is not required.

![Alt text](./images_README/classif_example1.png?raw=true "original vs classified image")

This classification also capture in and out of focus nucleation events (not yet tubules).
This could be size tresholded or circularity tresholded. 
(as microscope is not aligned, they tend to appear more int top left.)
Its very nice. Better than eyes.


# Results
You are still there?
Results are present in *ready#* folders
## Ready1
Skeletonized image.
Count the length of the path, you got an idea of the total length of microtubules.
The following image show you original and result.
The small nucleation (not yet tubules) are also captured. You can
set a treshold of size to avoid them.
This doesn't give you a count of the number of microtubules,
but a count of the total microtubule.
  

![Alt text](./images_README/ready1_example.png?raw=true "original vs skeletonized image")

The following image show you a superposition of 
in red the original image
and in blue the result from ready1 (dilated a little).
You can can see very few tubules are omitted.
The small blue dots are nucleations events.  
  
![Alt text](./images_README/supperpose_ready1+ori.png?raw=true "superposition")


## Ready2
A method to count the microtubules from segmented image could be implemented.
This would fit sticks into the obtained masks.
Or by separating sticks at intersections.
Can be done easely in Python, But in ImageJ I cannot do that.
Might be possible in Weka, but crossing sticks will always be an issue.

# Analyzed
Use skeleton analysis.
produce data tables.
name after the name of the s. s1, s2, s3, s4.
The name of the data should be of the same shape that the one provided.
i.e.  
**sample_4_s1_t24**  

This is the type of analysis proceeded:  
  
![Alt text](./images_README/skeleton_analysed.png?raw=true "superposition")
  
And the resulting data table.
Either just *s#.csv*.
with number of ends being, the number of blue pixels on the previous image
![Alt text](./images_README/data_ready.png?raw=true "superposition")

Or more detailled. There is also a plot
![Alt text](./images_README/endplot.png?raw=true "superposition")